const express = require('express');
const app = express();
const axios = require('axios');
const fs = require('fs');
const puerto = 8032;

const carpetapug = app.set('views', './views');
const pug = app.set('view engine', 'pug');

app.get('/', (req, res)=>{
  res.render('index', {titulo: "Pantalla principal"});
});

app.get('/version', (req, res)=>{
  axios.get('http://82.223.81.195:6601/version/')
  .then(response => {
    res.send(response.data);
  });
});

app.get('/buscar', (req, res)=>{
  res.render('formBusc', {titulo: "Buscar", texto: "Buscar", labelType: "Tipo a buscar: ", labelId: "ID a buscar: "});
});


app.get('/buscarOp', (req, res)=>{
  axios.get('http://82.223.81.195:6601/v2/entities/urn:ngsi-ld:'+req.query.type+':'+req.query.id+'?options=keyValues')
  .then(response => {
    res.send(response.data);
  })
  .catch(function (error){
    console.log(error);
    res.render('error', {titulo: "ERROR", mensaje:"ERROR" , texto: "No se encontró un elemento de ese tipo con ese id."});
  });
});

app.get('/borrar', (req, res)=>{
  res.render('formBorr', {titulo: "Borrar", texto: "Borrar", labelType: "Tipo: ", labelId: "ID del elemento a buscar: "});
});

app.get('/borrarOp', (req, res)=>{
  axios.delete('http://82.223.81.195:6601/v2/entities/urn:ngsi-ld:'+req.query.type+':'+req.query.id)
  .then(response => {
    res.render('error', {titulo: "Borrar", mensaje: "Hecho", texto: "El elemento de id "+req.query.id+" y tipo "+req.query.type+" ha sido borrado con éxito."});
  })
  .catch(function (error){
    console.log(error);
    res.render('error', {titulo: "ERROR", mensaje:"ERROR" , texto: "No se encontró un elemento de ese tipo con ese id."});
  });
});

app.get('/crear', (req, res)=>{
  res.render('formCre', {titulo: "Crear", texto: "Crear", labelType: "Tipo: ", labelId: "ID del elemento a crear: "});
});

app.get('/crearOp', (req, res)=>{
  axios.post('http://82.223.81.195:6601/v2/entities/?type='+req.query.type+'&options=keyValues', {
    id:"urn:ngsi-ld:"+req.query.type+":"+req.query.id, "type":req.query.type,
    name:{"type":"Text", "value":req.query.name},
    size:{"type":"Text", "value": req.query.size},
    price:{"type":"Integer", "value": req.query.price}
  })
  .then(response => {
    res.render('error', {titulo: "Crear", mensaje: "Hecho", texto: "Se ha creado el elemento con éxito."});
  })
  .catch(function (error){
    console.log(error);
    res.render('error', {titulo: "ERROR", mensaje:"ERROR" , texto: "Algo salió mal."});
  });
});

app.get('/actualizar', (req, res)=>{
  res.render('formAct', {titulo: "Actualizar", texto: "Actualizar", labelType: "Tipo: ", labelId: "ID del elemento a actualizar: "});
});

app.get('/actualizarOp', (req, res)=>{
  let name = req.query.name;
  axios.post('http://82.223.81.195:6601/v2/entities/urn:ngsi-ld:'+req.query.type+':'+req.query.id+'/attrs/name', {
    name
  })
  .then(response => {
    res.render('error', {titulo: "Actualizar", mensaje: "Hecho", texto: "Se ha modificado el nombre del elemento con éxito."});
  })
  .catch(function (error){
    console.log(error);
    res.render('error', {titulo: "ERROR", mensaje:"ERROR" , texto: "Algo salió mal."});
  });
});

app.listen(puerto, function(){
    console.log("El servidor está activo en http://localhost:8032/");
});